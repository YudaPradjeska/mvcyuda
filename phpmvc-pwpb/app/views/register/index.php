<link rel="stylesheet" href="<?=BASE_URL?>/css/bootstrap.css">
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="p-5">
                        <form action="<?=BASE_URL?>/register/sign" method="post">
                        <h1 class="text-center">Register</h1>
                            <div class="form-group mb-3">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" name="username" id="username">
                            </div>
                            <div class="form-group mb-3">
                                <label for="nama">Nama</label>
                                <input type="text" class="form-control" name="nama" id="nama">
                            </div>
                            <div class="form-group mb-3">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                            <div class="row">

                                <div class="col-md-6 form-group mb-3">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="cpassword">Confirm password</label>
                                <input type="password" class="form-control" name="cpassword" id="cpassword">
                            </div>
                                </div>
                            <div class="form-group mb-3 text-end">
                                <input type="submit" class="btn btn-primary" name="submit" value="Sign up">
                            </div>
                            <div class="text-center">
                                <a href="<?=BASE_URL?>/login">Already have an account?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>