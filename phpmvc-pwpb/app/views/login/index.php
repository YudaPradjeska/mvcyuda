<link rel="stylesheet" href="<?=BASE_URL?>/css/bootstrap.css">
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="p-5">
                            <form action="<?=BASE_URL?>/login/sign" method="post">
                                <h1 class="text-center">Login</h1>
                                <div class="form-group mb-3">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" name="username" id="username">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="form-group mb-3 text-end">
                                    <input type="submit" name="submit" value="Login" class="btn btn-primary">
                                </div>
                                <div class="text-center">
                                    <a href="<?=BASE_URL?>/register">Don't have an account?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>